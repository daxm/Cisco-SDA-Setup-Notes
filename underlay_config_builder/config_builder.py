#! /usr/bin/env python3

# from pprint import pprint
from ruamel.yaml import YAML
import jinja2
from pathlib import Path
# from underlay_config_builder.helpers import *
from helpers import *
from typing import Dict, Any
import logging
import ipaddress

# GLOBALS
YAML_CONFIGS_DIR = '.'
TEMPLATE_DIR = 'templates'
OUTPUT_CONFIGS_DIR = 'output_configs'

# Logging is better than printing!
logging.getLogger(__name__).addHandler(logging.NullHandler())
logging_format = '%(asctime)s - %(levelname)s:%(filename)s:%(lineno)s - %(message)s'
logging_dateformat = '%Y/%m/%d-%H:%M:%S'
logging_level = logging.INFO
logging_filename = 'log_output.log'
logging.basicConfig(format=logging_format,
                    datefmt=logging_dateformat,
                    filename=logging_filename,
                    filemode='w',
                    level=logging_level)

env = jinja2.Environment(loader=jinja2.FileSystemLoader(TEMPLATE_DIR), trim_blocks=True, lstrip_blocks=True)
env.filters['ip_netmask'] = ip_netmask
env.filters['ip_address'] = ip_address


def write_out_config(data_vars: Dict[str, Any]) -> None:
    template = env.get_template('master.j2')
    config = template.render(**data_vars)
    logging.info("Config for %s has been built.", data_vars['name'])

    # Now that the config has been built save it to the output file.
    path = Path(OUTPUT_CONFIGS_DIR) / f"{data_vars['name']}.cfg"
    with open(path, 'w') as stream:
        try:
            stream.write(config)
            logging.info("Config for %s has been written to '%s'.", data_vars['name'], path)
            print(f"Config for {data_vars['name']} has been built.")
        except OSError:
            logging.error("An error has occurred trying to open or write to: '%s'.", path)
            exit(1)


def build_config(data_vars: Dict[str, Any]) -> None:
    # collated dictionary of information from data_vars inputs.
    intermediary_dict = {'interface_assignments': []}

    # Build a unique list of all device names
    device_list = []
    for connection in data_vars['connections']:
        for device_name in connection:
            if device_name not in device_list:
                device_list.append(device_name)
    intermediary_dict['device_list'] = device_list

    # Assign Loopback interfaces /32 subnets from the loopback_subnets_pool
    ip_list = create_subnets(data_vars['loopback_subnets_pool'], mask=32)
    if len(device_list) > len(ip_list):
        msg = "Error:  Loopback Subnet is not large enough for the number of devices needing IPs."
        print(msg)
        logging.error(msg)
        exit(1)
    else:
        for pos, host in enumerate(device_list):
            intermediary_dict['interface_assignments'].append({'device': host,
                                                               'interface': 'loopback0',
                                                               'ip': ip_list[pos]})
    # Assign interconnections between devices /30 subnets from the p2p_subnets_pool
    ip_list = create_subnets(data_vars['p2p_subnets_pool'], mask=30)
    if len(data_vars['connections']) > len(ip_list):
        msg = "Error:  P2P Subnet is not large enough for the number of interconnections needing IPs."
        print(msg)
        logging.error(msg)
        exit(1)
    else:
        for pos, connection in enumerate(data_vars['connections']):
            ip_list1 = list(ipaddress.IPv4Network(ip_list[pos]).hosts())
            # First loop through this connection to build the description that will go on both device's interfaces.
            description_items = []
            for item in connection:
                description_items.append(f"{item} on {connection[item]}")

            for pos1, item in enumerate(connection):
                if pos1 == 0:
                    desc_pos = 1
                else:
                    desc_pos = 0
                description = f'To {description_items[desc_pos]}'
                intermediary_dict['interface_assignments'].append({'device': item,
                                                                   'interface': connection[item],
                                                                   'ip': f"{str(ip_list1[pos1])}/30",
                                                                   'description': description})

    # Parse intermediary_dict by device and send to write_config subroutine.
    for device in intermediary_dict['device_list']:
        device_config = {'name': device,
                         'interfaces': [],
                         'commands': data_vars['commands'],
                         'system_mtu': data_vars['system_mtu']}
        for interface in intermediary_dict['interface_assignments']:
            if device == interface['device']:
                device_config['interfaces'].append(interface)
                # The ISIS net value needs to be unique per device.
                if interface['interface'] == 'loopback0':
                    split_ip = interface['ip'].split('/')
                    device_config['isis'] = f'49.0001.{uniquify_isis_net_address_using_ip_address(split_ip[0])}.00'
        write_out_config(data_vars=device_config)


def main() -> None:
    # Load the config_vars YAML files into the border_routers and fusion_router variables
    yaml = YAML(typ='safe')
    path = Path(YAML_CONFIGS_DIR) / 'input_vars.yml'
    with open(path, 'r') as stream:
        try:
            logging.info("Loading '%s' file.", path)
            build_config(data_vars=yaml.load(stream))
        except OSError:
            logging.error("An error has occurred trying to open %s.", path)
            exit(1)


if __name__ == '__main__':
    main()
