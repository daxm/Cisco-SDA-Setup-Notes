import ipaddress
from typing import List
import logging


def uniquify_isis_net_address_using_ip_address(ip: str) -> str:
    # Convert a dotted decimal IP into a format that can be used to uniquify an ISIS net address.
    # Just to be obnoxious I'm going to also reverse the IP.
    # (i.e. Make '192.168.5.234' into 4325.0086.1291)
    tmp = ''
    for item in ip.split('.'):
        if len(item) < 3:
            if len(item) < 2:
                item = f'00{item}'
            else:
                item = f'0{item}'
        tmp += item
    isis_net_segment = ''
    for pos, str_char in enumerate(reversed(tmp)):
        isis_net_segment += str_char
        if pos % 4 == 3:
            isis_net_segment += '.'
    # Drop the last period off of the string and return it.
    return isis_net_segment[0:-1]


def create_subnets(subnet: str, mask: int) -> List[str]:
    logging.info("Explode an ip subnet into a list of its host IP addresses.")
    subnet_list = []
    ip_networks = ipaddress.IPv4Network(subnet).subnets(new_prefix=mask)
    for ip in ip_networks:
        subnet_list.append(str(ip))
    return subnet_list


def ip_netmask(ip: str) -> str:
    logging.info("Gathering netmask for %s.", ip)
    ipv4 = ipaddress.IPv4Interface(ip)
    return str(ipv4.netmask)


def ip_address(ip: str) -> str:
    logging.info("Gathering ip address for %s.", ip)
    ipv4 = ipaddress.IPv4Interface(ip)
    return str(ipv4.ip)
