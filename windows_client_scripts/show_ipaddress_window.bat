@echo off
Rem ##################################################################################################
Rem #     AUTHOR: ANAND KANANI (@ANKANANI)
Rem #     
Rem #     PURPOSE:
Rem #      THIS SCRIPT WILL SHOW A SMALL WINDOW WITH THE IP ADDRESS / CONNECTION STATE OF THE 
Rem #      ETHERNET AND WIFI INTERFACES OF THE HOST.
Rem #      WAIT FOR A FEW SECONDS FOR THE SCRIPT TO START AFTER DOUBLE-CLICKING ON THIS FILE
Rem #     
Rem ##################################################################################################

PowerShell -nop -exec bypass -win Hidden -noni -Command  "function get_hostname_and_ip {  $info = Get-NetIPConfiguration  -All | ? InterfaceDescription -NotLike '*Microsoft*';  $hostinfo = 'Hostname: ' + $env:computername + ' ' + [Environment]::NewLine;  $ipinfo = '';  foreach ( $line in $info ) {    if ($line.NetAdapter.Status -eq 'Up')   { $ipinfo = $ipinfo + $line.InterfaceAlias + ': ' +  $line.IPv4Address.IPAddress + ' ' + [Environment]::NewLine; }   else   { $ipinfo = $ipinfo + $line.InterfaceAlias + ': ' +  $line.NetAdapter.Status + ' ' + [Environment]::NewLine; }  }  return $hostinfo + $ipinfo; }  Add-Type -AssemblyName System.Windows.Forms; $Form = New-Object system.Windows.Forms.Form; $timer = New-Object System.Windows.Forms.Timer; $timer.Interval = 5000;  $Form.Text = 'Localhost IP Info'; $Form.AutoSize = $True; $Form.AutoSizeMode = 'GrowAndShrink'; $Form.BackColor = 'Black'; $Form.ForeColor = 'LightGreen'; $Form.ShowIcon = $False; $Form.TopMost = $True; $Font = New-Object System.Drawing.Font('Times New Roman',24); $Form.Font = $Font; $Label = New-Object System.Windows.Forms.Label; $Label.Text = get_hostname_and_ip; $Label.AutoSize = $True; $Form.Controls.Add($Label);  $timer.add_tick( {$Label.Text = get_hostname_and_ip; [GC]::Collect()} ); $timer.start();  $Form.ShowDialog() | out-null;"