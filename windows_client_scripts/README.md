FOR WINDOWS ONLY

This script will show a popup with the ip address / interface status information and refreshes every 5 seconds.

Just download and double-click on the ```.bat``` file. Note that it takes a few seconds for the GUI window to load after executing the script.

Below is a screenshot of how the output looks like.

![screencap](https://gitlab.com/daxm/Cisco-SDA-Setup-Notes/raw/master/windows_client_scripts/screencap.png)