# Cisco TSA's SDA Setup Notes
This repository contains our notes, tips, reminders, and tools on what to remember to do when setting up an SDA lab.


# How to get this repository
The best way is to clone a copy of it to your computer using GIT.  So, download and install a git client for your
PC's particular OS.  https://git-scm.com/downloads

**NOTE:  You need, at a minimum Python3 installed on your computer.  Having GIT installed as well is a plus.**

Once git is installed use it to clone this repository:
* From a command line, navigate to the folder where you want the repository to be cloned.
* Issue the command:

```bash
git clone https://gitlab.com/daxm/Cisco-SDA-Setup-Notes.git

```
This will copy the whole repository to a folder called Cisco-SDA-Setup-Notes.

At a later time, if you need to refresh your local copy of this repository issue the following command:
```bash
git pull

```

# Requirements for using this repository
Python3 is required to run the config_builder.py file.
You'll need to install python3 to your computer. https://www.python.org/downloads/

Once python is installed, navigate to the Cisco-SDA-Setup-Notes folder and issue the command:
```bash
pip3 install -r requirements.txt

```

## BGP Config Builder
The config_builder.py file in the bgp_config_builder directory is used to take the information populated in the 
**input_vars.yaml** file and make a copy/paste snippet of configuration code for the BGP peerings needed to connect the 
SDA border router(s) to the fusion router.

This config builder is **VERY** specific to these caveats:
* 1 fusion and 2 border routers.
* The devices **MUST** be named: Fusion, Border1, and Border2.
* The keyword "GRT" is used to indicate the Global Routing Table if/where a VRF is being referenced but you want to
reference the global routing table.

### Steps:
* **Copy** the **input_vars.yml-example** file to **input_vars.yml** and then modify input_vars.yaml with your specific 
information for your lab environment.
* To build the configs from a command line, in the bgp_config_builder folder, issue the command:

```bash
python3 config_builder.py

```
* **Open up** the output files in the **output_configs directory** and copy/paste that into your border/fusion routers.
(Note:  Please read the config to sanity check it prior to copy/pasting.)


## Underlay Config Builder
The config_builder.py file in the underlay_config_builder directory is used to take the information populated in the 
**input_vars.yaml** file and make a copy/paste snippet of configuration code for the devices used to make the underlay
ISIS network.

### Steps:
* **Copy** the **input_vars.yml-example** file to **input_vars.yml** and then modify input_vars.yaml with your specific 
information for your lab environment.
* To build the configs from a command line, in the underlay_config_builder folder, issue the command:

```bash
python3 config_builder.py

```
* **Open up** the output files in the **output_configs directory** and copy/paste that into your fabric underlay
 routers.
(Note:  Please read the config to sanity check it prior to copy/pasting.)


