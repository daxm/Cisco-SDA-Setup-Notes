import ipaddress
from typing import Dict, Any, Tuple, List
import logging


def ip_netmask(ip: str) -> str:
    logging.info("Gathering netmask for %s.", ip)
    ipv4 = ipaddress.IPv4Interface(ip)
    return str(ipv4.netmask)


def ip_address(ip: str) -> str:
    logging.info("Gathering ip address for %s.", ip)
    ipv4 = ipaddress.IPv4Interface(ip)
    return str(ipv4.ip)


def split_name(name: str) -> Tuple[str, str, str]:
    logging.info("Splitting name into constituent parts for %s.", name)
    names, vrf = name.split("__")
    left_name, right_name = names.split("-")
    return str(left_name), str(right_name), str(vrf)


def load_vars(items: Dict[Dict[str, Any], Any], device_name: str) -> list:
    logging.info("Loading specific vars related to %s.", device_name)
    return_list = []
    for item in items:
        # Split the "name" field into it's constituent parts.
        left_name, right_name, vrf = split_name(item['name'])
        if left_name == device_name or right_name == device_name:
            item['left_name'] = left_name
            item['right_name'] = right_name
            item['vrf'] = vrf
            return_list.append(item)
    return return_list


def collect_vlans(router: List[Dict[str, Any]]) -> List[Dict[str, Any]]:
    logging.info("Collecting VLANs.")
    vlans = []
    for item in router:
        if 'vlan' in item:
            vlans.append({'vlan': item['vlan'], 'name': item['name']})
    return vlans


def collect_trunk_interfaces(interfaces: List[str]) -> List[str]:
    logging.info("Collecting Trunk Interfaces.")
    trunks = []
    for interface in interfaces:
        trunks.append(interface)
    return trunks


def collect_routed_interfaces(router: List[Dict[str, Any]],
                              this_device: str,
                              interface_list: str,
                              output_format: str) -> List[Dict[str, Any]]:
    logging.info("Collecting Routed Interfaces for %s.", this_device)
    interfaces = []
    for item in router:
        interface = {}
        neighbor_device = get_neighbor_device(this_device=this_device, name=item['name'])
        interface_name = interface_list[0]
        if len(interface_list) > 1 and get_device_type(this_device) == 'fusion' and neighbor_device == 'Border2':
            interface_name = interface_list[1]
        ip = host_address(ip_subnet=item['ip'], this_device=this_device, neighbor_device=neighbor_device)
        netmask = ip_netmask(item['ip'])
        ip = f'{ip}/{netmask}'
        if output_format == 'router':
            interface['name'] = f"{interface_name}.{item['vlan']}"
            interface['encapsulation'] = f"{item['vlan']}"
        else:
            interface['name'] = f"vlan {item['vlan']}"
        vrf = get_vrf(item['name'])
        interface['ip'] = ip
        interface['description'] = item['name']
        if vrf == 'GRT':
            pass
        else:
            interface['vrf'] = vrf
        interfaces.append(interface)
    return interfaces


def host_address(ip_subnet: str, this_device: str, neighbor_device: str) -> str:
    logging.info("Calculate Host Address for %s from %s.", this_device, ip_subnet)
    ip_position = 0
    if get_device_type(this_device) == 'border' \
            and (get_device_type(neighbor_device) == 'fusion' or this_device == 'Border2'):
        ip_position = 1
    ip_range = ipaddress.ip_network(ip_subnet).hosts()
    for position, host_addr in enumerate(ip_range):
        if position == ip_position:
            return host_addr


def neighbor_address(ip_subnet: str, this_device: str, neighbor_device: str) -> str:
    logging.info("Calculate Neighbor Address for %s from %s.", this_device, ip_subnet)
    ip_position = 1
    if get_device_type(this_device) == 'border' \
            and (get_device_type(neighbor_device) == 'fusion' or this_device == 'Border2'):
        ip_position = 0
    ip_range = ipaddress.ip_network(ip_subnet).hosts()
    for position, host_addr in enumerate(ip_range):
        if position == ip_position:
            return str(host_addr)


def get_neighbor_device(this_device: str, name: str) -> str:
    logging.info("Gather neighbor device for %s from %s.", this_device, name)
    left_name, right_name, vrf = split_name(name)
    if this_device == left_name:
        return right_name
    else:
        return left_name


def collect_bgp_neighbors(router: List[Dict[str, Any]],
                          this_device: str,
                          border_asn: str,
                          fusion_asn: str,
                          vrf_name: str = 'GRT') -> List[Dict[str, Any]]:
    logging.info("Collecting GRT BGP neighbors for %s.", this_device)
    bgp = []
    for item in router:
        left_name, right_name, test_vrf = split_name(item['name'])
        if test_vrf == vrf_name:
            neighbor_device = get_neighbor_device(this_device=this_device, name=item['name'])
            if 'border' == get_device_type(neighbor_device):
                asn = border_asn
            else:
                asn = fusion_asn
            ip = neighbor_address(ip_subnet=item['ip'], this_device=this_device, neighbor_device=neighbor_device)
            bgp.append({'asn': asn, 'ip': ip})
    return bgp


def collect_bgp_vrf(router: List[Dict[str, Any]],
                    vrfs: List[str],
                    this_device: str,
                    border_asn: str,
                    fusion_asn: str) -> List[str]:
    logging.info("Collecting VRF BGP neighbors for %s.", this_device)
    new_vrfs = []
    for vrf in vrfs:
        current_vrf = vrf
        current_vrf['bgp'] = {}
        current_vrf['bgp']['neighbors'] = collect_bgp_neighbors(router=router,
                                                                this_device=this_device,
                                                                vrf_name=current_vrf['name'],
                                                                border_asn=border_asn,
                                                                fusion_asn=fusion_asn)
        new_vrfs.append(current_vrf)
    return new_vrfs


def get_device_type(device: str) -> str:
    logging.info("Calculate device type for %s.", device)
    if device == 'Fusion':
        return 'fusion'
    return 'border'


def get_vrf(name: str) -> str:
    logging.info("Gather VRF from %s.", name)
    left_name, right_name, vrf = split_name(name)
    return vrf


def collect_vrfs(data_vars: List[Dict[str, Any]]) -> List[Dict[str, Any]]:
    logging.info("Collecting VRFs from the 'name' fields.")
    vrfs_list = []
    for item in data_vars:
        left_name, right_name, vrf = split_name(item['name'])
        if vrf not in vrfs_list and vrf != 'GRT':
            vrfs_list.append(vrf)
    # Convert to a list of dictionaries
    vrfs = []
    for item in vrfs_list:
        vrfs.append({'name': item})
    return vrfs

