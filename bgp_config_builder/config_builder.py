#! /usr/bin/env python3

# This script assumes the following, for now:
# That you are doing VRF-lite between the fusion and border routers.

# from pprint import pprint
from ruamel.yaml import YAML
import jinja2
from pathlib import Path
#from underlay_config_builder.helpers import *
from helpers import *
from typing import Dict, Any
import logging

# GLOBALS
YAML_CONFIGS_DIR = '.'
TEMPLATE_DIR = 'templates'
OUTPUT_CONFIGS_DIR = 'output_configs'
DEVICE_LIST = ['Border1', 'Border2', 'Fusion']

# Logging is better than printing!
logging.getLogger(__name__).addHandler(logging.NullHandler())
logging_format = '%(asctime)s - %(levelname)s:%(filename)s:%(lineno)s - %(message)s'
logging_dateformat = '%Y/%m/%d-%H:%M:%S'
logging_level = logging.INFO
logging_filename = 'log_output.log'
logging.basicConfig(format=logging_format,
                    datefmt=logging_dateformat,
                    filename=logging_filename,
                    filemode='w',
                    level=logging_level)

env = jinja2.Environment(loader=jinja2.FileSystemLoader(TEMPLATE_DIR), trim_blocks=True, lstrip_blocks=True)
env.filters['ip_netmask'] = ip_netmask
env.filters['ip_address'] = ip_address


def write_out_config(data_vars: Dict[str, Any]) -> None:
    template = env.get_template('master.j2')
    config = template.render(**data_vars)
    logging.info("Config for %s has been built.", data_vars['name'])

    # Now that the config has been built save it to the output file.
    path = Path(OUTPUT_CONFIGS_DIR) / f"{data_vars['name']}_as_a_{data_vars['output_format']}.cfg"
    with open(path, 'w') as stream:
        try:
            stream.write(config)
            logging.info("Config for %s has been written to '%s'.", data_vars['name'], path)
            print(f"Config for {data_vars['name']} has been built.")
        except OSError:
            logging.error("An error has occurred trying to open or write to: '%s'.", path)
            exit(1)


def build_config(data_vars: Dict[str, Any], output_format: str = 'switch') -> None:
    # data_vars is a jumble of data to build the border and fusion configs.
    vrfs = collect_vrfs(data_vars=data_vars['data'])
    """
    device_vars is a list of the items from the data_vars['data'] file but it gets passed around to the subroutines a 
    lot.
    device_dict is a dictionary of the "finished product" that gets send to the writer subroutine.
    """
    # Parse the data_vars to pull the needed information for each device.
    for device in DEVICE_LIST:
        device_vars = load_vars(data_vars['data'], device)

        # Organize vars into something useful
        device_dict = {'name': device,
                       'border_asn': data_vars['border_bgp_asn'],
                       'fusion_asn': data_vars['fusion_bgp_asn'],
                       'output_format': output_format}
        if get_device_type(device) == 'border':
            device_dict['bgp_asn'] = device_dict['border_asn']
        else:
            device_dict['bgp_asn'] = device_dict['fusion_asn']

        # Build list of VRFs being used
        device_dict['vrfs'] = vrfs

        # Collect VLANs
        if output_format == 'switch':
            device_dict['vlans'] = collect_vlans(device_vars)

        # Collect Trunked Interfaces
        if output_format == 'switch':
            device_dict['trunks'] = collect_trunk_interfaces(interfaces=data_vars['border_interfaces'])

        # Collect Routed Interfaces
        if output_format == 'switch':
            interface_list = ['vlan']
        else:
            if get_device_type(device) == 'border':
                interface_list = data_vars['border_interfaces']
            else:
                interface_list = data_vars['fusion_interfaces']
        device_dict['interfaces'] = collect_routed_interfaces(router=device_vars,
                                                              this_device=device,
                                                              interface_list=interface_list,
                                                              output_format=output_format)

        # Collect BGP GRT
        device_dict['bgp'] = {}
        device_dict['bgp']['neighbors'] = collect_bgp_neighbors(router=device_vars,
                                                                this_device=device,
                                                                border_asn=device_dict['border_asn'],
                                                                fusion_asn=device_dict['fusion_asn'])

        # Collect VRF BGP
        device_dict['vrfs'] = collect_bgp_vrf(router=device_vars,
                                              vrfs=device_dict['vrfs'],
                                              this_device=device,
                                              border_asn=device_dict['border_asn'],
                                              fusion_asn=device_dict['fusion_asn'])

        # Add verification commands
        device_dict['commands'] = data_vars['commands']

        # Save built configuration to the output file.
        write_out_config(device_dict)


def main() -> None:
    # Load the config_vars YAML files into the border_routers and fusion_router variables
    yaml = YAML(typ='safe')
    path = Path(YAML_CONFIGS_DIR) / 'input_vars.yml'
    with open(path, 'r') as stream:
        try:
            thedata = yaml.load(stream)
            logging.info("Loading '%s' file.", path)
            build_config(thedata, output_format='switch')
            build_config(thedata, output_format='router')
        except OSError:
            logging.error("An error has occurred trying to open %s.", path)
            exit(1)


if __name__ == '__main__':
    main()
