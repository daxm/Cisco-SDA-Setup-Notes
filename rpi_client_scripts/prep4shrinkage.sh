#!/usr/bin/env bash

CWD=pwd
echo "Cleaning /"
cd /
cat /dev/zero > zero.fill;sync;sleep 1;sync;rm -r zero.fill
echo "Cleaning /boot"
cd /boot
cat /dev/zero > zero.fill;sync;sleep 1;sync;rm -r zero.fill
cd $CWD
