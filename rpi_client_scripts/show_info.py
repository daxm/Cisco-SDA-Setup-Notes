#! /usr/bin/python3

import socket
import threading
import time
from tkinter import *
import netifaces

# ******************** #
# --- User Modifiable ---
# ******************** #

# Enable/Disable refreshing of IPs periodically
refresh_screen = True
refresh_rate = 5

# Window decorations
bg_color = 'black'
text_color = 'green'
my_font = 'helvetica 20 bold'
width = 425
height_multiplier = 40

# Window's Title
title = "Localhost Info"


# ******************** #
# --- Functions ---
# ******************** #


# Exit program when window is closed
def on_closing():
    root.destroy()


# Collect hostname
def collect_hostname():
    try:
        hostname = socket.gethostname()
    except:
        hostname = "Undefined"
    text = 'Hostname: ' + hostname
    return text


# Collect IP for specific NIC
def collect_ip(nic='eth0'):
    int_ip = "NIC disconnected"
    nic_info = netifaces.ifaddresses(nic)
    try:
        if 'addr' in nic_info[netifaces.AF_INET][0]:
            int_ip = nic_info[netifaces.AF_INET][0]['addr']
    except:
        int_ip = "No IP"
    return int_ip


# Refreshing the listed IP addresses
def refresh_ips(rate=2):
    def run():
        while refresh_screen is True:
            # Running the functions
            print("Refreshing interfaces.")
            show_interfaces()
            time.sleep(rate)

    thread = threading.Thread(target=run)
    thread.start()


# Setting Window location
def place_window(width=300, height=200):
    # get screen width and height
    screen_width = root.winfo_screenwidth()
    screen_height = root.winfo_screenheight()

    # calculate position x and y coordinates
    x = (screen_width / 2) - (width / 2)
    y = (screen_height / 2) - (height / 2)
    root.geometry('%dx%d+%d+%d' % (width, height, x, y))


def show_hostname():
    # Hostname
    my_hostname = Label(root, text=collect_hostname(), fg=text_color)
    my_hostname.grid(sticky=W, row=0, column=0, padx=5)


def show_interfaces():
    # Get Interfaces and their IP Addresses
    labels = {}
    for idx, interface in enumerate(netifaces.interfaces()):
        text = interface + ':                                  '
        lb = Label(root, text=text, fg=text_color)
        lb.grid(sticky=W, row=(idx + 1), column=0, padx=5)
        labels[interface] = lb
    for idx, interface in enumerate(netifaces.interfaces()):
        text = interface + ': ' + collect_ip(nic=interface)
        lb = Label(root, text=text, fg=text_color)
        lb.grid(sticky=W, row=(idx + 1), column=0, padx=5)
        labels[interface] = lb


# ******************** #
# --- Create Window ---
# ******************** #

# Create a default window
root = Tk()

# Change window coloring and font size/type
root.option_add("*Font", my_font)
root.option_add("*Background", bg_color)
root.configure(background=bg_color)

# Configure what to do if window is closed
root.protocol("WM_DELETE_WINDOW", on_closing)


# ******************** #
# --- Display Text ---
# ******************** #

show_hostname()
# Let Docker load fully before displaying interfaces.
time.sleep(30)
show_interfaces()


# ******************** #
# --- Display Window ---
# ******************** #

# Placing the window
place_window(width=width, height=(1 + len(netifaces.interfaces())) * height_multiplier)

# Title of windwow
root.title(title)

# Start refresh cycle and then display window
refresh_ips(rate=refresh_rate)
root.mainloop()
