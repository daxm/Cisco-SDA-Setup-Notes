#! /usr/bin/env python3

# from pprint import pprint
from ruamel.yaml import YAML
from pathlib import Path
from typing import Dict, Any, List
import logging
import json

# GLOBALS
YAML_CONFIGS_DIR = '.'
YAML_CONFIG_FILES = ('env.yml', 'userdata.yml')
POSTMAN_DIRECTORY = 'postman_files'
POSTMAN_ENVIRONMENT_FILE = 'postman_environment.json'
POSTMAN_COLLECTIONS_FILE = 'postman_collections.json'
PYTHON_SCRIPTS_DIRECTORY = 'python_scripts'

# Logging is better than printing!
logging.getLogger(__name__).addHandler(logging.NullHandler())
logging_format = '%(asctime)s - %(levelname)s:%(filename)s:%(lineno)s - %(message)s'
logging_dateformat = '%Y/%m/%d-%H:%M:%S'
logging_level = logging.INFO
logging_filename = 'log_output.log'
logging.basicConfig(format=logging_format,
                    datefmt=logging_dateformat,
                    filename=logging_filename,
                    filemode='w',
                    level=logging_level)


def recursive_items(test_value: Any, name: str = ''):
    keypath = f'{name}'
    if type(test_value) is dict:
        for key, value in test_value.items():
            if keypath is '':
                name = f'{key}'
            else:
                name = f'{keypath}_{key}'
            if type(value) is dict:
                yield from recursive_items(value, name)
            elif type(value) is list:
                yield from recursive_items(value, name)
            else:
                yield (name, value)
    elif type(test_value) is list:
        for idx, item in enumerate(test_value):
            keypath_list = f'{keypath}-{idx + 1}'
            if type(item) is dict or type(item) is list:
                yield from recursive_items(item, keypath_list)
            else:
                yield (keypath_list, item)
    else:
        yield (keypath, test_value)


def postman_environment_template(key: str = '',
                                 value: str = '',
                                 description: str = 'Built by Python Script.',
                                 enabled=True):
    return {'key': key, 'value': value, 'description': description, 'enabled': enabled}


def postman_collection_item_template(name: str = '', item: List = None):
    if item is None:
        item = []
    return {'name': name, 'item': item}


def postman_collection_item__entity_template(name: str = '',
                                             event: List = None,
                                             request: Dict[str, Any] = None,
                                             response: List = None):
    if request is None:
        request = {}
    if event is None:
        event = []
    if response is None:
        event = []
    return {'name': name, 'event': event, 'request': request, 'response': response}


def build_postman_environment_variables(data_vars: Dict[str, Any] = None) -> Dict:
    if data_vars is None:
        data_vars = {}
    postman_var = {
        'id': '',
        'name': 'DNAC',
        'values': []
    }

    # Convert yml file variables (loaded as a dictionary in Python) into a new dict ready to be exported into json.
    for k, v in recursive_items(data_vars):
        postman_var['values'].append(postman_environment_template(key=k, value=v))

    return postman_var


def build_postman_collections(data_vars: Dict[str, Any] = None) -> Dict:
    if data_vars is None:
        data_vars = {}
    postman_coll = {
        'info': {
            '_postman_id': '',
            'name': 'DNAC',
            'schema': 'https://schema.getpostman.com/json/collection/v2.1.0/collection.json'
        },
        'item': {},
        'event': {},
        'variable': {}
    }

    print(f'{data_vars}')
    return postman_coll


def main() -> None:
    # Load the config YAML files into the user_vars variable.
    yaml = YAML(typ='safe')
    user_vars = {}
    for yml_file in YAML_CONFIG_FILES:
        path = Path(YAML_CONFIGS_DIR) / yml_file
        with open(path, 'r') as stream:
            try:
                logging.info("Loading '%s' file.", path)
                user_vars.update(yaml.load(stream))
            except OSError:
                logging.error("An error has occurred trying to open %s.", path)
                exit(1)

    # Use user_vars to build postman environment variables.
    postman_vars = build_postman_environment_variables(data_vars=user_vars)
    if POSTMAN_ENVIRONMENT_FILE:
        path = Path(POSTMAN_DIRECTORY) / POSTMAN_ENVIRONMENT_FILE
        with open(path, 'w') as f:
            logging.info("Dumping user_vars into POSTMAN_ENVIRONMENT_FILE.")
            json.dump(postman_vars, f)

    # Use user_vars to build postman collections
    if POSTMAN_COLLECTIONS_FILE:
        path = Path(POSTMAN_DIRECTORY) / POSTMAN_COLLECTIONS_FILE
        with open(path, 'w') as f:
            logging.info("Dumping collection_vars into POSTMAN_COLLECTIONS_FILE.")
            json.dump(build_postman_collections(data_vars=postman_vars), f)


if __name__ == '__main__':
    logging.info("Starting Program.")
    main()
    logging.info("Program End.")
