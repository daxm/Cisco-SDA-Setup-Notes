# DNAC API Uses

## How to use:
1.  Copy the *-example files to new file names (same names but without the -example on the end).
2.  Edit the newly created files and make them fit your desired configuration.
3.  Run the ``build_postman_and_python_files.py`` file to build custom output files with your custom built configurations.
4.  Either run the python scripts or import the postman files into postman and run them there.
