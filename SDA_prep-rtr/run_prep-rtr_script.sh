#!/usr/bin/env bash

echo
echo "WARNING:  Running this script will default the edge-1, core, fusion, cp-border-1, and cp-border-2 devices back to the BEGINNING of the demo."
read -r -n 1 -p "Are you sure you want to do this? [y/n]: " REPLY
case $REPLY in
  [yY])
    echo
    echo "Allrightythen...  Here we go!"

    cd ~/Cisco-SDA-Setup-Notes/SDA_prep-rtr
    ansible-playbook prep-rtr.yml
    ;;

  *)
    echo
    echo "Whew!  Dodged a bullet there, didn't you!"
    echo
    ;;
esac

echo
read -n 1 -s -r -p "-----> Review the output of the script for any errors or messages, then press any key to continue. <-----"
echo
