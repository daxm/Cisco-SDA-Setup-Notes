#!/usr/bin/env bash

cd ~/Cisco-SDA-Setup-Notes/SDA_prep-rtr
ansible-playbook dnac-reachability-test.yml

echo
read -n 1 -s -r -p "-----> Review the output of the script for any errors or messages, then press any key to continue. <-----"
echo
